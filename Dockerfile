# Grab the upstream Kaniko image
FROM gcr.io/kaniko-project/executor:v1.8.1 as upstreamKaniko

# Grab the upstream manifest-tool image
FROM mplatform/manifest-tool:v2.0.3 as upstreamMT

# Grab the upstream Crane image
FROM gcr.io/go-containerregistry/crane:v0.10.0 as upstreamCrane


# Build our final image
FROM gitlab-registry.cern.ch/linuxsupport/cs9-base

RUN dnf upgrade -y \
  && dnf install -y git jq \
  && dnf clean all

# Install https://github.com/GoogleContainerTools/kaniko
COPY --from=upstreamKaniko /kaniko/executor /kaniko/
COPY --from=upstreamKaniko /kaniko/ssl/ /kaniko/ssl/
COPY --from=upstreamKaniko /kaniko/.docker/ /kaniko/.docker/
ENV PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/kaniko
ENV SSL_CERT_DIR=/kaniko/ssl/certs
ENV DOCKER_CONFIG /kaniko/.docker/

# Install https://github.com/estesp/manifest-tool
COPY --from=upstreamMT /manifest-tool /usr/local/bin/

# Install https://github.com/google/go-containerregistry/tree/main/cmd/crane
COPY --from=upstreamCrane /ko-app/crane /usr/local/bin/
